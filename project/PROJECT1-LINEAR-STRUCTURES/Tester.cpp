#include "Tester.hpp"
#include<iostream>
using namespace std;




void Tester::RunTests()
{
	Test_IsEmpty();
	Test_IsFull();
	Test_Size();
	Test_GetCountOf();
	Test_Contains();

	Test_PushFront();
	Test_PushBack();

	Test_Get();
	Test_GetFront();
	Test_GetBack();

	Test_PopFront();
	Test_PopBack();
	Test_Clear();

	Test_ShiftRight();
	Test_ShiftLeft();

	Test_Remove();
	Test_Insert();
}

void Tester::DrawLine()
{
	cout << endl;
	for (int i = 0; i < 80; i++)
	{
		cout << "-";
	}
	cout << endl;
}

void Tester::Test_Init()
{
	DrawLine();
	cout << "TEST: Test_Init" << endl;

	List<string>listA;
	
}

void Tester::Test_ShiftRight()
{
	DrawLine();
	cout << "TEST: Test_ShiftRight" << endl;

	
	List<int> listA;
	listA.PushBack(5);
	listA.PushBack(6);
	listA.PushBack(7);
	listA.PushBack(8);
	cout << "unshifted array: ";
	TestDisplayList(listA);
	cout << "test1-this test is shifting right from index 0" << endl;
	cout << "expectedList = 5  6 7 7 8" << endl;
	if (listA.ShiftRight(2) == true)
	{
		cout << "actualList = ";
		TestDisplayList(listA);
	}
	else
	{
		cout << "list was not shifted" << endl;
	}
	cout << "test2-trying to shift a full list" << endl;
	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		listA.PushBack(i);
	}

	if (listA.ShiftRight(20) == true)
	{
		cout << "actualList = ";
		TestDisplayList(listA);
	}
	else
	{
		cout << "list was not shifted" << endl;
	}

	

}

void Tester::Test_ShiftLeft()
{
	DrawLine();
	cout << "TEST: Test_ShiftLeft" << endl;
	cout << "test1-shift left from position 5 to the end of the list" << endl;
	List<int> listA;
	
	for (int i = 0; i <= 10; i++)
	{
		listA.PushBack(i);
	}

	cout << "unshifted list: ";
	TestDisplayList(listA);
	if (listA.ShiftLeft(5) == true)
	{
		cout << "actualList = ";
		TestDisplayList(listA);
	}
	else
	{
		cout << "list was not shifted" << endl;
	}

	cout << "test2-shift left from position 0 to the end of the list" << endl;


	for (int i = 0; i <= 50; i++)
	{
		listA.PushBack(i);
	}

	cout << "unshifted list: ";
	TestDisplayList(listA);
	if (listA.ShiftLeft(0) == true)
	{
		cout << "actualList = ";
		TestDisplayList(listA);
	}
	else
	{
		cout << "list was not shifted" << endl;
	}
	

	
}

void Tester::Test_Size()
{
	DrawLine();
	cout << "TEST: Test_Size" << endl;
	cout << "Prerequisite: Implement PushBack" << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;
		int expectedSize = 0;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		testList.PushBack(1);

		int expectedSize = 1;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_IsEmpty()
{
	DrawLine();
	cout << "TEST: Test_IsEmpty" << endl;

	
	{
		// Test 1
		cout << endl << "Test 1" << endl;
		List<int> listA;
		bool expectedValue = true;
		bool actualValue = listA.IsEmpty();

		cout << "Created list, didn't add anything, should be empty..." << endl;
		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 1" << endl;
		List<int> listA;
		listA.PushBack(5);
		bool expectedValue = false;
		bool actualValue = listA.IsEmpty();

		cout << "Created list, added one thing, shouldn't be empty..." << endl;
		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
}

void Tester::Test_IsFull()
{
	DrawLine();
	cout << "TEST: Test_IsFull" << endl;

	List<int>listA;
	cout << "test 1:testing empty list to see if full" << endl;
	if (listA.IsFull())
	{
		cout << "list is full" << endl;
	}
	else
	{
		cout << "list is not full-test was successful" << endl;
	}
	cout << "test 2: testing full list to see if full" << endl;
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			listA.PushBack(2);
		}
		if (listA.IsFull())
		{
			cout << "list is full-test was successful" << endl;
		}
		else
		{
			cout << "list is not full" << endl;
		}

}

void Tester::Test_PushFront()
{
	DrawLine();
	cout << "TEST: Test_PushFront" << endl;

	cout << endl << "Test 1, create a list and insert one item, PushFront() should return true" << endl;

	List<string> testlist;
	bool expectedValue = true;
	bool actualValue = testlist.PushFront("A");

	cout << "Expected value: " << expectedValue << endl;
	cout << "Actual value:   " << actualValue << endl;

	if (expectedValue == actualValue)
	{
		cout << "Test passed" << endl;
	}
	else
	{
		cout << "Test failed" << endl;
	}



	cout << endl << "Test 1b, create a list and insert one item, Size() should return 1" << endl;

	List<string> testlistA;
	testlistA.PushFront("A");

	 expectedValue = 1;
	 actualValue = testlist.Size();

	cout << "Expected value: " << expectedValue << endl;
	cout << "Actual value:   " << actualValue << endl;

	if (expectedValue == actualValue)
	{
		cout << "Test passed" << endl;
	}
	else
	{
		cout << "Test failed" << endl;
	}



	cout << endl << "Test 2, create a list, insert 101 items, PushFront() should return false" << endl;

	List<string> testlistB;
	for (int i = 0; i < 100; i++)
	{
		testlistB.PushFront("Z");
	}

	bool expectedValueA = false;
	bool actualValueA = testlistB.PushFront("A");

	cout << "Expected value: " << expectedValueA << endl;
	cout << "Actual value:   " << actualValueA << endl;
	
	if (expectedValueA == actualValueA)
	{
		cout << "Test passed" << endl;
	}
	else
	{
		cout << "Test failed" << endl;
	}



	cout << endl << "Test 2b, create a list, insert 101 items, Size() should return 100" << endl;

	List<string> testlistC;
	for (int i = 0; i < 100; i++)
	{
		testlistC.PushFront("Z");
	}
	testlistC.PushFront("A");

	int  expectedValueB = 100;
	 int actualValueB = testlistC.Size();

	cout << "Expected value: " << expectedValueB << endl;
	cout << "Actual value:   " << actualValueB << endl;

	if (expectedValueB == actualValueB)
	{
		cout << "Test passed" << endl;
	}
	else
	{
		cout << "Test failed" << endl;
	}



	cout << endl << "Test 3, insert 'A', 'B', 'C'" << endl;

	List<string> testlistD;
	testlistD.PushFront("A");
	testlistD.PushFront("B");
	testlistD.PushFront("C");

	string expectedValue_0 = "C";
	string expectedValue_1 = "B";
	string expectedValue_2 = "A";

	string* actualValue_0 = testlistD.Get(0);
	string* actualValue_1 = testlistD.Get(1);
	string* actualValue_2 = testlistD.Get(2);

	if (actualValue_0 == nullptr || actualValue_1 == nullptr || actualValue_2 == nullptr)
	{
		cout << "no segfaults plz" << endl;
		return;
	}

	cout << "Expected value at 0: " << expectedValue_0 << endl;
	cout << "Expected value at 1: " << expectedValue_1 << endl;
	cout << "Expected value at 2: " << expectedValue_2 << endl;

	cout << "Actual value at 0: " << *actualValue_0 << endl;
	cout << "Actual value at 1: " << *actualValue_1 << endl;
	cout << "Actual value at 2: " << *actualValue_2 << endl;

	if (expectedValue_0 != *actualValue_0 ||
		expectedValue_1 != *actualValue_1 ||
		expectedValue_2 != *actualValue_2)
	{
		cout << "Failed" << endl;
	}
	else
	{
		cout << "Pass" << endl;
	}


}

void Tester::Test_PushBack()
{
	DrawLine();
	cout << "TEST: Test_PushBack" << endl;

	// Put tests here
	{
		cout << endl << "Test 1, create a list and insert one item, PushBack() should return true" << endl;

		List<string> testlist;
		bool expectedValue = true;
		bool actualValue = testlist.PushBack("A");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 1b, create a list and insert one item, Size() should return 1" << endl;

		List<string> testlist;
		testlist.PushBack("A");

		int expectedValue = 1;
		int actualValue = testlist.Size();

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2, create a list, insert 101 items, PushBack() should return false" << endl;

		List<string> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack("Z");
		}

		bool expectedValue = false;
		bool actualValue = testlist.PushBack("A");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2b, create a list, insert 101 items, Size() should return 100" << endl;

		List<string> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack("Z");
		}
		testlist.PushBack("A");

		int expectedValue = 100;
		int actualValue = testlist.Size();

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 3, insert 'A', 'B', 'C', make sure 0 = A, 1 = B, 2 = C" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		string expectedValue_0 = "A";
		string expectedValue_1 = "B";
		string expectedValue_2 = "C";

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		string* actualValue_2 = testlist.Get(2);

		if (actualValue_0 == nullptr || actualValue_1 == nullptr || actualValue_2 == nullptr)
		{
			cout << "no segfaults plz" << endl;
			return;
		}

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;
		cout << "Expected value at 2: " << expectedValue_2 << endl;

		cout << "Actual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;
		cout << "Actual value at 2: " << *actualValue_2 << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedValue_2 != *actualValue_2)
		{
			cout << "Failed" << endl;
		}
		else
		{
			cout << "Pass" << endl;
		}
	}
}

void Tester::Test_PopFront()
{
	DrawLine();
	cout << "TEST: Test_PopFront" << endl;

	List<int>listA;
	cout << " test 1-calling popfront with no items in list" << endl;
	if (listA.PopFront())
	{
		cout << "test was not successful" << endl;
	}
	else
	{
		cout << "test was  successful" << endl;
	}
	cout << " test 2-calling popfront with one item in list" << endl;
	listA.PushBack(2);
	if (listA.PopFront())
	{
		cout << "test was successful" << endl;
	}
	else
	{
		cout << "was not successful" << endl;
	}
	cout << " test 3-calling popfront with five items in list" << endl;
	listA.PushBack(1);
	listA.PushBack(2);
	listA.PushBack(3);
	listA.PushBack(4);
	listA.PushBack(5);
	if (listA.PopFront())
	{
		cout << "test was successful" << endl;
	}
	else
	{
		cout << "was not successful" << endl;
	}
}

void Tester::Test_PopBack()
{
	DrawLine();
	cout << "TEST: Test_PopBack" << endl;

	List<int>listA;
	cout << " test 1-calling popback with no items in list" << endl;
	if (listA.PopBack())
	{
		cout << "test was not successful" << endl;
	}
	else
	{
		cout << "test was  successful" << endl;
	}
	cout << " test 2-calling popback with one item in list" << endl;
	listA.PushBack(2);
	if (listA.PopBack())
	{
		cout << "test was successful" << endl;
	}
	else
	{
		cout << "was not successful" << endl;
	}
	cout << " test 3-calling popback with five items in list" << endl;
	listA.PushBack(1);
	listA.PushBack(2);
	listA.PushBack(3);
	listA.PushBack(4);
	listA.PushBack(5);
	if (listA.PopBack())
	{
		cout << "test was successful" << endl;
	}
	else
	{
		cout << "was not successful" << endl;
	}
}

void Tester::Test_Clear()
{
	DrawLine();
	cout << "TEST: Test_Clear" << endl;

	List<string>listA;
	cout << "test 1-make a list with a b c in it" << endl;
	listA.PushBack("A");
	listA.PushBack("B");
	listA.PushBack("C");
	listA.Clear();
	if (listA.Size() == 0)
	{
		cout << "test was a success" << endl;
	}
	else
	{
		cout << "test was no a success" << endl;
	}
	List<int>listB;

	cout << "test 2-sending an empty list to clear" << endl;
	if (listB.Size() == 0)
	{
		cout << "test was a success" << endl;
	}
	else
	{
		cout << "test was no a success" << endl;
	}
}

void Tester::Test_Get()
{
	DrawLine();
	cout << "TEST: Test_Get" << endl;

	List<int>listA;
	
	cout << "test 1-list empty calling get function" << endl;
	if (listA.Get(2) == nullptr)
	{
		cout << "test is a success" << endl;
	}
	else
	{
		cout << "test failed" << endl;
	}

	cout << "test 2-3 items in list calling get function" << endl;
	listA.PushBack(1);
	listA.PushBack(2);
	listA.PushBack(3);
	if (*listA.Get(2) == 3)
	{
		cout << "test is a success" << endl;
	}
	else
	{
		cout << "test failed" << endl;
	}

	cout << "test 3-3 items in list calling get function with index > 100" << endl;
	
	if (*listA.Get(102) == 3)
	{
		cout << "test is not a success" << endl;
	}
	else
	{
		cout << "test passed" << endl;
	}







}

void Tester::Test_GetFront()
{
	DrawLine();
	cout << "TEST: Test_GetFront" << endl;

	List<int>listA;

	cout << "test 1-list empty calling get function" << endl;
	if (listA.GetFront() == nullptr)
	{
		cout << "test is a success" << endl;
	}
	else
	{
		cout << "test failed" << endl;
	}

	cout << "test 2-5 items in list calling get function" << endl;
	listA.PushBack(1);
	listA.PushBack(2);
	listA.PushBack(3);
	listA.PushBack(4);
	listA.PushBack(5);
	if (*listA.GetFront() == 1)
	{
		cout << "test is a success" << endl;
	}
	else
	{
		cout << "test failed" << endl;
	}

	

}

void Tester::Test_GetBack()
{
	DrawLine();
	cout << "TEST: Test_GetBack" << endl;

	List<int>listA;

	cout << "test 1-list empty calling get function" << endl;
	if (listA.GetBack() == nullptr)
	{
		cout << "test is a success" << endl;
	}
	else
	{
		cout << "test failed" << endl;
	}

	cout << "test 2-5 items in list calling get function" << endl;
	listA.PushBack(1);
	listA.PushBack(2);
	listA.PushBack(3);
	listA.PushBack(4);
	listA.PushBack(5);
	if (*listA.GetBack() == 5)
	{
		cout << "test is a success" << endl;
	}
	else
	{
		cout << "test failed" << endl;
	}
}

void Tester::Test_GetCountOf()
{
	DrawLine();
	cout << "TEST: Test_GetCountOf" << endl;
	
	List<string> listA;
	listA.PushBack("A");
	listA.PushBack("B");
	listA.PushBack("C");
	cout << "origninal list" << endl;
	TestDisplayListString(listA);
	cout << "count of A in list: " << listA.GetCountOf("A") << endl;
	List<string> listB;
	cout << "origninal list" << endl;
	TestDisplayListString(listB);
	cout << "count of A in list: " << listB.GetCountOf("A") << endl;
	List<string> listC;
	listC.PushBack("A");
	listC.PushBack("B");
	listC.PushBack("C");
	cout << "origninal list" << endl;
	TestDisplayListString(listC);
	cout << "count of D in list: " << listC.GetCountOf("D") << endl;
	List<string> listD;
	listD.PushBack("A");
	listD.PushBack("B");
	listD.PushBack("C");
	listD.PushBack("A");
	cout << "origninal list" << endl;
	TestDisplayListString(listD);
	cout << "count of A in list: " << listD.GetCountOf("A") << endl;
}

void Tester::Test_Contains()
{
	DrawLine();
	cout << "TEST: Test_Contains" << endl;

	List<string> listA;
	listA.PushBack("A");
	listA.PushBack("B");
	listA.PushBack("C");

	
	cout << "test 1: checking for 'A' ";
	cout << "original list ";
	TestDisplayListString(listA);

	if (listA.Contains("A") == true)
	{
		cout << "string is in the list" << endl;
	}
	else
	{
		cout << "string is not in list" << endl;
	}
	cout << "test 2: checking for 'AB' ";
	cout << "original list ";
	TestDisplayListString(listA);

	if (listA.Contains("AB") == true)
	{
		cout << "string is in the list" << endl;
	}
	else
	{
		cout << "string is not in list" << endl;
	}

}

void Tester::Test_Remove()
{
	DrawLine();
	cout << "TEST: Test_Remove" << endl;

	List<string>listA;

	listA.PushBack("A");
	listA.PushBack("B");
	listA.PushBack("C");
	listA.PushBack("A");
	cout << "test 1- created a list with 4 items and two A's and removed them" << endl;
	cout << "original list" << endl;
	TestDisplayListString(listA);
	if(listA.Remove("A"))
	{
		cout << "test was successful" << endl;
	}
	else
	{
		cout << "test was not successful" << endl;
	}
	TestDisplayListString(listA);
	cout << "test 2- created a list with 2 items and trying to remove an item not in that list" << endl;
	TestDisplayListString(listA);
	
	if (listA.Remove("D"))
	{
		cout << "test was not successful" << endl;
	}
	else
	{
		cout << "test was successful" << endl;
	}

}

void Tester::Test_Insert()
{
	DrawLine();
	cout << "TEST: Test_Insert" << endl;

	List<string> listA;
	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		listA.PushBack("A");
	}
	cout << "test 1- trying beyond size of list" << endl;
	cout << "original list: ";
	TestDisplayListString(listA);

	if (listA.Insert(102, "B"))
	{
		cout << "it worked" << endl;
		TestDisplayListString(listA);

	}
	else
	{
		cout << "it did not work" << endl;
	}
	cout << "test 2- invalid position" << endl;
	cout << "original list: ";
	TestDisplayListString(listA);
	if (listA.Insert(-100, "B"))
	{
		cout << "it worked" << endl;
		TestDisplayListString(listA);

	}
	else
	{
		cout << "it did not work" << endl;
	}
	List<string> listB;
	for (int i = 0; i < 20; i++)
	{
		listB.PushBack("A");
	}
	cout << "test 3- inserting at position 3" << endl;
	cout << "original list: ";
	TestDisplayListString(listB);
	
	if (listB.Insert(3, "B"))
	{
		cout << "it worked" << endl;
		TestDisplayListString(listB);

	}
	else
	{
		cout << "it did not work" << endl;
	}
	
}
void Tester::TestDisplayList(List<int> list1)
{
	
	
	for (int i = 0; i < list1.Size()+1; i++)
	{
		cout << *list1.Get(i) << " ";
		
	}
	cout << endl;
}

void Tester::TestDisplayListString(List<string> list2)
{
	cout << " list: ";
	for (int i = 0; i < list2.Size(); i++)
	{
		cout << *list2.Get(i) << " ";

	}
	cout << endl;
}
