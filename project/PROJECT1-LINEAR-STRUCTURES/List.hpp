#ifndef _LIST_HPP
#define _LIST_HPP

#include <iostream>
using namespace std;

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
	// private member variables
	int m_itemCount;
	T m_arr[ARRAY_SIZE];            // Add this!

									// functions for interal-workings
	bool ShiftRight(int atIndex)
	{
		if (IsFull() || IsEmpty())
		{
			return false; 
		}
		else if (atIndex < 0 || atIndex >= m_itemCount)
		{
			return false;
		}
		for (int i = m_itemCount; i > atIndex; i--)
		{
			m_arr[i] = m_arr[i - 1];
		
		}
		return true;
	}

	bool ShiftLeft(int atIndex)
	{
		if (IsEmpty())
		{
			return false;
		}
		if (atIndex < 0 || atIndex >= m_itemCount-1)
		{
			return false;
		}
		for (int i = atIndex; i < m_itemCount-1; i++)
		{
			m_arr[i] = m_arr[i+1];
		}
		m_itemCount--;
		return true;

	}

public:
	List()
	{
		m_itemCount = 0;
	}

	~List()
	{
	}

	// Core functionality
	int     Size() const
	{
		return m_itemCount;
	}

	bool    IsEmpty() const
	{
		return (m_itemCount == 0);
	}

	bool    IsFull() const
	{
		return (m_itemCount == ARRAY_SIZE);
	}

	bool    PushFront(const T& newItem)
	{
		if (IsFull()) 
		{
			return false;
		}
		//  shift everything to the right
		
			
		if (IsEmpty())
		{
			m_arr[0] = newItem;
			m_itemCount++;
			return true;
		}
		else if(ShiftRight(0))
		{
			m_arr[0] = newItem;
			m_itemCount++;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool    PushBack(const T& newItem)
	{
		//        int m_itemCount;
		//        T m_arr[ARRAY_SIZE];

		if (IsFull())
		{
			return false;   // Insert failed
		}

		m_arr[m_itemCount] = newItem;
		m_itemCount++;
		return true;
	}

	bool    Insert(int atIndex, const T& item)
	{
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}
		if (atIndex == m_itemCount)
		{
			m_arr[atIndex] = item;
		}
		else
		{
			if (ShiftRight(atIndex) == true)
				m_arr[atIndex] = item;
			else
			{
				cout << "couldn't shift right-didn't inseert element" << endl;
				return false;
			}
		}
		
		m_itemCount++;
		return true;
	}

	bool    PopFront()
	{
		if (m_itemCount == 1)
		{
			m_itemCount--;
			return true;
		}
		if (ShiftLeft(1))
		{
			m_itemCount--;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool    PopBack()
	{
		if (IsEmpty())
		{
			return false;
		}
		m_itemCount--;
		return true; 
	}

	bool    Remove(const T& item)
	{
		if (IsEmpty())
		{
			return false;
		}

		int count = GetCountOf(item);

		if (count == 0)
		{
			return false;
		}
		for (int i = 0; i < count; i++)
		{
			bool found = false;
			for (int j = 0; !found && j < m_itemCount; j++)
			{
				if (m_arr[j] == item)
				{
					if (j == Size() - 1)
					{
						m_itemCount--;
						return true;
					}
					else if (ShiftLeft(j))
					{
						
						found = true;
					}
					else
						return false;
				}
			}
		}
		return true;
	
	}

	bool    Remove(int atIndex)
	{
		if (IsEmpty())
		{
			return false;
		}
		if (atIndex < 0 || atIndex >= m_itemCount)
		{
			return false;
		}
		if (ShiftLeft(atIndex))
		{
			//m_itemCount--;
			return true;
		}
		return false;

	}

	void    Clear()
	{
		m_itemCount = 0;
	}

	// Accessors
	T*      Get(int atIndex)
	{
		if (IsEmpty())
		{
			return nullptr;
		}
		else
		{
			return &m_arr[atIndex];
		}
	}

	T*      GetFront()
	{
		if (IsEmpty())
		{
			return nullptr;
		}
		else
		{
			return &m_arr[0];
		}
	}

	T*      GetBack()
	{
		if (IsEmpty())
		{
			return nullptr;
		}
		else
		{
			return  &m_arr[m_itemCount - 1];
		}
	}

	// Additional functionality
	int     GetCountOf(const T& item) const
	{
		if (IsEmpty())
		{
			return 0;
		}
		int count = 0;
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
				count++;
		}
		return count;
	}

	bool    Contains(const T& item) const
	{
		if (IsEmpty())
		{
			return false;
		}
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
				return true;
		}
	
        return false;
	
	}

	friend class Tester;
};


#endif
