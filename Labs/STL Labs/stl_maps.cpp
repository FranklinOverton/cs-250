// Lab - Standard Template Library - Part 5 - Maps
// Franklin Overton

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	map<char, string> colors;


	char color;
	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['m'] = "FF00FF";
	colors['y'] = "FFFF00";

	
	bool done = false;

	while (!done)
	{
		cout << "Enter a color letter, or 'q' to stop: ";
		cin >> color;
		if (color == 'q')
		{
			done = true;
		}
		else
		{
			cout << "Hex: " << colors[color] << endl;
		}
	}

	cout << "Goodbye";

    cin.ignore();
    cin.get();
    return 0;
}

/*
Enter a color letter, or 'q' to stop: r
Hex: FF0000
Enter a color letter, or 'q' to stop: g
Hex: 00FF00
Enter a color letter, or 'q' to stop: b
Hex: 0000FF
Enter a color letter, or 'q' to stop: c
Hex: 00FFFF
Enter a color letter, or 'q' to stop: m
Hex: FF00FF
Enter a color letter, or 'q' to stop: y
Hex: FFFF00
Enter a color letter, or 'q' to stop: t
Hex:
Enter a color letter, or 'q' to stop: q
Goodbye


*/