// Lab - Standard Template Library - Part 4 - Stacks
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> word;
	bool done = false;
	string letter;

	cout << "Enter the next letter of the word, or UNDO to undo, or DONE to stop." << endl;
	while (!done) {
		
		cout << ">> ";
		cin >> letter;

		if (letter == "UNDO")
		{
			cout << "Removed " << word.top() << endl;
			word.pop();
		}
		else if (letter == "DONE")
		{
			done = true;
		}
		else
		{
			word.push(letter);
		}
	}
	cout << "Finished word: ";
	while (!word.empty())
	{
		cout << word.top();
		word.pop();
	}
    cin.ignore();
    cin.get();
    return 0;
}

/*
Enter the next letter of the word, or UNDO to undo, or DONE to stop.
>> e
>> r
>> r
>> UNDO
Removed r
>> e
>> h
>> t
>> -
>> h
>> UNDO
Removed h
>> o
>> l
>> l
>> e
>> h
>> DONE
Finished word: hello-there


*/