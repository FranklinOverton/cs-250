// Lab - Standard Template Library - Part 3 - Queues
// Franklin Overton

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;
	bool done = false;
	int selection;
	float amount;
	float balance = 0;
	while (!done) {

		cout << "-----------------------------------" << endl
			<< "Transactions queued: " << transactions.size() << endl
			<< "1. Enqueue transaction         2. Continue" << endl
			<< ">> ";
		
		cin >> selection;

		if (selection == 1) {
			cout << "Enter amount (positive or negative) for next transaction: ";
			cin >> amount;
			transactions.push(amount);
		}
		else if (selection == 2) {
			done = true;
		}

	}
	while (!transactions.empty()) {
        cout << transactions.front() << " pushed to account" << endl;
		balance += transactions.front();
		transactions.pop();
	}
	cout << "Final balance: $" << balance << endl
		<< "Goodbye";

    cin.ignore();
    cin.get();
    return 0;
}

/*
-----------------------------------
Transactions queued: 0
1. Enqueue transaction         2. Continue
>> 1
Enter amount (positive or negative) for next transaction: 9.99
-----------------------------------
Transactions queued: 1
1. Enqueue transaction         2. Continue
>> 1
Enter amount (positive or negative) for next transaction: -5.20
-----------------------------------
Transactions queued: 2
1. Enqueue transaction         2. Continue
>> 1
Enter amount (positive or negative) for next transaction: 15.38
-----------------------------------
Transactions queued: 3
1. Enqueue transaction         2. Continue
>> 1
Enter amount (positive or negative) for next transaction: -2.33
-----------------------------------
Transactions queued: 4
1. Enqueue transaction         2. Continue
>> 2
9.99 pushed to account
-5.2 pushed to account
15.38 pushed to account
-2.33 pushed to account
Final balance: $17.84
Goodbye


*/
