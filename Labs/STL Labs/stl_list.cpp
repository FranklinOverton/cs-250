// Lab - Standard Template Library - Part 2 - Lists
// Franklin Overton

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list < string >& states)
{
 for (list <string >::iterator it = states.begin();
	 it != states.end();
	 it++)
	{
	 cout << *it << "\t";
	}
	 cout << endl;
 }

int main()
{
	list <string> states;
	bool cont = false;
	int selection;
	string newState;

	while (!cont) {
		cout << "-------------------------------" << endl
			<< " " << endl
			<< "State list size: " << states.size() << endl
			<< "1. add new state to front      2. add new state to back" << endl
			<< "3. pop front state             4. pop back state" << endl
			<< "5. continue" << endl << endl
			<< ">> ";
		cin >> selection;

		if (selection == 1) {
			cout << "ADD STATE TO FRONT" << endl
				<< "Enter new state name: ";
			cin.ignore();
			getline(cin, newState);
			states.push_front(newState);

		}
		else if (selection == 2) {
			cout << "ADD STATE TO BACK" << endl
				<< "Enter new state name: ";
			cin.ignore();
			getline(cin, newState);
			states.push_back(newState);
		}
		else if (selection == 3) {
			cout << "REMOVE STATE FROM FRONT" << endl
				<< states.front() << " removed" << endl;
				
			
			states.pop_front();
		}
		else if (selection == 4) {
			cout << "REMOVE STATE FROM BACK" << endl
				<< states.back() << " removed" << endl;


			states.pop_back();
		}
		else if (selection == 5) {
			cont = true;
		}

	} // end while loop

	cout << "ORIGINAL LIST: " << endl;
	DisplayList(states);


	cout << "REVERSED LIST: " << endl;
	states.reverse();
	DisplayList(states);


	cout << "SORTED LIST: " << endl;
	states.sort();
	DisplayList(states);


	cout << "REVERSE-SORTED LIST: " << endl;
	states.reverse();
	DisplayList(states);

	cout << "Goodbye";


    cin.ignore();
    cin.get();
    return 0;
}

/*
-------------------------------

State list size: 0
1. add new state to front      2. add new state to back
3. pop front state             4. pop back state
5. continue

>> 1
ADD STATE TO FRONT
Enter new state name: Missouri
-------------------------------

State list size: 1
1. add new state to front      2. add new state to back
3. pop front state             4. pop back state
5. continue

>> 1
ADD STATE TO FRONT
Enter new state name: Nebraska
-------------------------------

State list size: 2
1. add new state to front      2. add new state to back
3. pop front state             4. pop back state
5. continue

>> 2
ADD STATE TO BACK
Enter new state name: Kansas
-------------------------------

State list size: 3
1. add new state to front      2. add new state to back
3. pop front state             4. pop back state
5. continue

>> 2
ADD STATE TO BACK
Enter new state name: Alaska
-------------------------------

State list size: 4
1. add new state to front      2. add new state to back
3. pop front state             4. pop back state
5. continue

>> 3
REMOVE STATE FROM FRONT
Nebraska removed
-------------------------------

State list size: 3
1. add new state to front      2. add new state to back
3. pop front state             4. pop back state
5. continue

>> 4
REMOVE STATE FROM BACK
Alaska removed
-------------------------------

State list size: 2
1. add new state to front      2. add new state to back
3. pop front state             4. pop back state
5. continue

>> 1
ADD STATE TO FRONT
Enter new state name: Iowa
-------------------------------

State list size: 3
1. add new state to front      2. add new state to back
3. pop front state             4. pop back state
5. continue

>> 2
ADD STATE TO BACK
Enter new state name: California
-------------------------------

State list size: 4
1. add new state to front      2. add new state to back
3. pop front state             4. pop back state
5. continue

>> 5
ORIGINAL LIST:
Iowa    Missouri        Kansas  California
REVERSED LIST:
California      Kansas  Missouri        Iowa
SORTED LIST:
California      Iowa    Kansas  Missouri
REVERSE-SORTED LIST:
Missouri        Kansas  Iowa    California
Goodbye


*/
