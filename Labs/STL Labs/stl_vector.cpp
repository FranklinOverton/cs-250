// Lab - Standard Template Library - Part 1 - Vectors
// FIRSTNAME, LASTNAME
//Franklin Overton

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector<string> courses; // string[] courses
	bool done = false;

	while (!done)
	{
		cout << endl << endl << "main menu" << endl;
		cout << "1. add a new course"
			<< " 2. remove the last course"
			<<  " 3. display the course list"
			<< " 4.quit" << endl;

		int choice;
		cin >> choice;
		if (choice == 1) { //add course
			string courseName;
			cout << "please enter the course: ";
			cin.ignore();
			getline(cin, courseName);
			

			//cin >> courseName;
			courses.push_back(courseName); // add to list of courses
		}
		else if (choice == 2) { //remove course
			courses.pop_back(); // remove last item in list of courses
		}
		else if (choice == 3) { //display list
			for (unsigned int i = 0; i < courses.size(); i++)
			{
				cout << i << ". " << courses[i] << endl;
			}
		}
		else if (choice == 4) { //quit
			done = true;
		}
	}
    cin.ignore();
    cin.get();
    return 0;
}
