#ifndef _PRIORITY_QUEUE
#define _PRIORITY_QUEUE

#include <iostream>
#include <iomanip>
using namespace std;

#include "Menu.hpp"
#include "Job.hpp"

const int MAX_SIZE = 1000;

class PriorityQueue
{
    public:
    PriorityQueue();
    void Push( Job* task );
    void Pop();
    Job* Front();
    int Size();
    void Display();

    private:
    void ShiftRight( int atIndex );
    void ShiftLeft( int atIndex );

    Job* m_arr[MAX_SIZE];
    int m_itemCount;
};

PriorityQueue::PriorityQueue()
{
    m_itemCount = 0;
}

void PriorityQueue::Push( Job* task )
{
	cout << endl << endl;
	Menu::Header("insert new task");
	if (m_itemCount == MAX_SIZE)
	{
		throw runtime_error("Item cannot be added");
	}
	int insertAt = 0;
		cout << endl << "new item's due date: " << task ->dueOnDay << endl;
	for (int i = 0; i < m_itemCount; i++)
	{
		if (m_arr[i]->dueOnDay > task->dueOnDay)
		{
			insertAt = i;
			break;
		}
		}
	cout << "insert at position " << insertAt << endl;

	ShiftRight(insertAt);
	m_arr[insertAt] = task;
	m_itemCount++;
	
	cout << endl << "queue state: " << endl;
	cout << "size: " << m_itemCount << endl;
	Display();


	/*if (m_itemCount == 0)//no queue
	{
		m_arr[0] = task;
		m_itemCount++;
	}
	//if m_arr is full then throw a runtime error
	else if (m_itemCount == MAX_SIZE)
	{
		throw runtime_error("Item cannot be added");
	}
	else
	{
		int i = 0;
		bool found = false;
		while (!found && i < m_itemCount)
		{
			if (m_arr[i]->dueOnDay > task->dueOnDay)
			{
				ShiftRight(i);
				found = true;
				m_arr[i] = task;
				m_itemCount++;
				break; // Done with loop

			}
			i++;
		
		}
	}*/
   
}

void PriorityQueue::Pop()
{
	//shift array to the left writing over the first job
	//if (m_itemCount > 0)
	
		ShiftLeft(0);
		m_itemCount--;
	

}
Job* PriorityQueue::Front()
{
	return m_arr[0];
}

void PriorityQueue::ShiftRight( int atIndex )
{
    for ( int i = m_itemCount; i > atIndex; i-- )
    {
        m_arr[i] = m_arr[i-1];
    }
}

void PriorityQueue::ShiftLeft( int atIndex )
{
    for ( int i = atIndex + 1; i < m_itemCount; i++ )
    {
        m_arr[i-1] = m_arr[i];
    }
}

int PriorityQueue::Size()
{
    return m_itemCount;
}

void PriorityQueue::Display()
{
    cout << endl;
    Menu::Header( "Priority Queue" );
    cout << left
        << setw( 10 ) << "INDEX"
        << setw( 10 ) << "DUE"
        << setw( 50 ) << "ASSIGNMENT" << endl;

    for ( int i = 0; i < m_itemCount; i++ )
    {
        cout << left
            << setw( 10 ) << i
            << setw( 10 ) << m_arr[i]->dueOnDay
            << setw( 50 ) << m_arr[i]->assignment << endl;
    }
}

#endif
