#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stack>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses()
{
    Menu::Header( "VIEW COURSES" );
	for (int i = 0; i < m_courses.Size(); i++)
		cout <<left <<setw(4) << i
		<<setw(10) << m_courses[i].code
	<<	setw(50) << m_courses[i].name
		<< m_courses[i].prereq << endl;
}

Course CourseCatalog::FindCourse(const string& code)
{
	for (int i = 0; i < m_courses.Size(); i++)
	{
		if (m_courses[i].code == code)
		{
			return m_courses[i];
		}
	}

	throw CourseNotFound("Cannot find course " + code + "!");
}

void CourseCatalog::ViewPrereqs()
{
    Menu::Header( "GET PREREQS" );

	string code;
	cout << "enter class code: ";
	cin >> code;

	LinkedStack<Course> prereqs;
	Course current;
	try
	{
		current = FindCourse(code);
	}
	catch (CourseNotFound ex)
	{
		cout << ex.what() << endl;
		return;
	}
	while (current.prereq != "")
	{
		prereqs.Push(current);
		try
		{
			current = FindCourse(current.prereq);
		}
		catch (CourseNotFound ex)
		{
			cout << ex.what() << endl;
			break;
		}
	}
	while (prereqs.IsEmpty() == false)
	{
		cout << prereqs.Top().code << "\t\t"
			<< prereqs.Top().name << endl;

			prereqs.Pop();
	}
	cout << endl;
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
